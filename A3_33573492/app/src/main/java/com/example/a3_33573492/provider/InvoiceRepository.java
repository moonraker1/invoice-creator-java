package com.example.a3_33573492.provider;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class InvoiceRepository {

    private InvoiceDao mInvoiceDao;
    private LiveData<List<Invoice>> mAllInvoices;

    InvoiceRepository(Application application) {
        InvoiceDatabase db = InvoiceDatabase.getDatabase(application);
        mInvoiceDao = db.invoiceDao();
        mAllInvoices = mInvoiceDao.getAllInvoices();
    }
    LiveData<List<Invoice>> getAllInvoices() {
        return mAllInvoices;
    }
    void insert(Invoice invoice) {
        InvoiceDatabase.databaseWriteExecutor.execute(() -> mInvoiceDao.AddInvoice(invoice));
    }

    void delete(Invoice invoice) {
        InvoiceDatabase.databaseWriteExecutor.execute(() -> mInvoiceDao.DeleteInvoice(invoice));
    }
}