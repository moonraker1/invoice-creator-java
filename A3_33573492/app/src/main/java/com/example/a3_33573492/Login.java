package com.example.a3_33573492;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username = findViewById(R.id.loginText);
        password = findViewById(R.id.passwordText);

        sharedPreferences = getSharedPreferences("CREDENTIALS", MODE_PRIVATE);

        //username.setHint(loginSaved);
        //password.setHint(passwordSaved);
    }

    public void checkCredentials(View view){
        try{
            String textUsername = username.getText().toString();
            String textPassword = password.getText().toString();
            String usernameSaved = sharedPreferences.getString("KEY_USERNAME", "NOT REGISTERED!!!").toString();
            String passwordSaved = sharedPreferences.getString("KEY_PASSWORD", "NOT REGISTERED!!!").toString();

            if (textUsername.equals(usernameSaved) && textPassword.equals(passwordSaved)){
                Intent newIntent = new Intent(this, MainActivity.class);
                startActivity(newIntent);
                finish();
            }
            else{
                Toast.makeText(this, "INCORRECT USERNAME OR PASSWORD", Toast.LENGTH_LONG).show();

            }
        }
        catch(Exception e){
            Toast.makeText(this, "Username/password are missing", Toast.LENGTH_LONG).show();

        }

    }

    public void backOnClick(View v){
        Intent i = new Intent(this, Registration.class);
        startActivity(i);
    }
}