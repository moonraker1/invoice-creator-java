package com.example.a3_33573492.provider;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface InvoiceDao {

    @Insert
    void AddInvoice(Invoice invoice);

    @Delete
    void DeleteInvoice(Invoice invoice);

    @Query("select * from invoices")
    LiveData<List<Invoice>> getAllInvoices();




    //  @Query("select name from fleets")
    //  LiveData<List<String>> getAllFleetNames();


}
