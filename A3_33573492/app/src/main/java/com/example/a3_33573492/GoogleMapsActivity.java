package com.example.a3_33573492;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class GoogleMapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private Geocoder geocoder;
    private Marker marker;

    SupportMapFragment mapFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map);
        geocoder = new Geocoder(this, Locale.getDefault());

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync((OnMapReadyCallback) GoogleMapsActivity.this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        LatLng melbourne = new LatLng(-37.814, 144.96332);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(melbourne));

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                //save current location
                String msg;
                boolean actionFlag;
                String selectedCountry = "";


                List<Address> addresses = new ArrayList<>();
                try {
                    //The results of getFromLocation are a best guess and are not guaranteed to be meaningful or correct.
                    // It may be useful to call this method from a thread separate from your primary UI thread.
                    addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1); //last param means only return the first address object
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

                if (addresses.size() == 0) {
                    msg = "No Country at this location!! Sorry";
                    actionFlag = false;
                }
                else {
                    android.location.Address address = addresses.get(0);
                    if (marker != null) {
                        marker.remove();
                    }
                    selectedCountry = address.getCountryName();
                    marker = Objects.requireNonNull(googleMap.addMarker(new MarkerOptions().position(point).title(selectedCountry)));
                    msg = "This is " + address.getCountryName() + ".";
                    actionFlag = true;
                }

                Snackbar.make(mapFragment.getView(), msg, Snackbar.LENGTH_LONG).show(); //.setAction("Details", (actionFlag) ? (new ActionOnClickListener(selectedCountry)) : null)
            }
        });
    }

    //Custom onclicklistener to accept 'selectedcountry' as parameter
    public class ActionOnClickListener implements View.OnClickListener {

        String country;

        public ActionOnClickListener(String country) {
            this.country = country; //this refers to the nested class's instance not the an instance of the enclosing class
        }

        @Override
        public void onClick(View v) {
           // Intent intent = new Intent(mapFragment.getContext(), CountryDetails.class);
           // intent.putExtra("country", country);
           // startActivity(intent);
        }
    }
}