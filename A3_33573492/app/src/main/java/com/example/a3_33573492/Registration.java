package com.example.a3_33573492;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registration extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    EditText textUsername, password1, password2;
    Button b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        sharedPreferences = getSharedPreferences("CREDENTIALS", MODE_PRIVATE);

        b = findViewById(R.id.signUpButton);
        textUsername = findViewById(R.id.editUsername);
        password1 = findViewById(R.id.editPassword1);
        password2 = findViewById(R.id.editPassword2);
        password2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String password = password1.getText().toString();
                String confirmPassword = password2.getText().toString();

                if (password.equals(confirmPassword)) {
                    password1.setBackgroundColor(Color.GREEN);
                    password2.setBackgroundColor(Color.GREEN);

                } else {
                    password1.setBackgroundColor(Color.WHITE);
                    password2.setBackgroundColor(Color.RED);
                }
            }
        });

    }
    private void saveLoginInfo(String username, String password){


        SharedPreferences.Editor e = sharedPreferences.edit();
        e.putString("KEY_USERNAME", username);
        e.putString("KEY_PASSWORD", password);
        e.apply();
    }
    public void signUpClick(View view){
        String username = textUsername.getText().toString();
        String password = password1.getText().toString();
        String finalPassword = password2.getText().toString();

        if (username.length() == 0 || password.length() == 0 || finalPassword.length() == 0){
            Toast.makeText(this, "Login/Password were not typed in", Toast.LENGTH_LONG).show();
        }
        else{
            if (!password.equals(finalPassword)){
                Toast.makeText(this, "Passwords do not match", Toast.LENGTH_LONG).show();
            }
            else{
                saveLoginInfo(username, finalPassword);
                Intent newIntent = new Intent(this, Login.class);
                startActivity(newIntent);
            }
        }


    }

    public void signInClick(View view){
        Intent newIntent = new Intent(this, Login.class);
        startActivity(newIntent);
    }
}