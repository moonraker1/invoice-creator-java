package com.example.a3_33573492;

import com.example.a3_33573492.provider.Invoice;
import com.example.a3_33573492.provider.InvoiceViewModel;
import com.example.a3_33573492.provider.Item;

import android.content.BroadcastReceiver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GestureDetectorCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.Random;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener{

    EditText issuerName, buyerName, buyerAddress, itemName, itemQuantity, itemCost;
    TextView total;

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    Switch isPaid;
    private DrawerLayout drawerlayout;
    private NavigationView navigationView;
    Toolbar toolbar;

    FloatingActionButton fab;


    RecyclerView recyclerViewItems;
    //RecyclerView recyclerViewInvoices;
    LinearLayoutManager layoutManagerItems, layoutManagerInvoices;

    ItemAdapter itemAdapter;
    InvoiceAdapter invoiceAdapter;
    ArrayList<Item> itemsOfInvoice= new ArrayList<>();
    ArrayList<Invoice> invoices = new ArrayList<>();

    InvoiceViewModel mInvoiceViewModel;

    GestureDetectorCompat gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_SMS}, 0);
        MyBroadcastReceiver myBroadCastReceiver = new MyBroadcastReceiver();
        registerReceiver(myBroadCastReceiver, new IntentFilter(MyReceiver.SMS_FILTER), Context.RECEIVER_EXPORTED);

        //RECYCLER VIEW DECLARATIONS
        recyclerViewItems = findViewById(R.id.rv1);
        //recyclerViewInvoices = findViewById(R.id.rv2);

        layoutManagerItems = new LinearLayoutManager(this);
        //layoutManagerInvoices = new LinearLayoutManager(this);


        recyclerViewItems.setLayoutManager(layoutManagerItems);
        //NOT SURE ABOUT THIS
        //recyclerViewInvoices.setLayoutManager(layoutManagerInvoices);

        itemAdapter = new ItemAdapter();
        itemAdapter.setData(itemsOfInvoice);
        recyclerViewItems.setAdapter(itemAdapter);

        invoiceAdapter = new InvoiceAdapter();
        invoiceAdapter.setData(invoices);
        //recyclerViewInvoices.setAdapter(invoiceAdapter);

        mInvoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        mInvoiceViewModel.getAllInvoices().observe(this, newData ->{
            invoiceAdapter.setData((ArrayList<Invoice>) newData);
            invoiceAdapter.notifyDataSetChanged();
        });




        MyGestureListener myGestureListener = new MyGestureListener();
        gestureDetector = new GestureDetectorCompat(this, myGestureListener);

        View myLayout = findViewById(R.id.layout_main);
        myLayout.setOnTouchListener(this);


        //INPUT FIELDS DECLARATION
        issuerName = findViewById(R.id.editIssuerNameText);
        buyerName = findViewById(R.id.editBuyerNameText);
        buyerAddress = findViewById(R.id.editAddressText);
        itemName = findViewById(R.id.editItemNameText);
        itemQuantity = findViewById(R.id.editQuantityText);
        itemCost = findViewById(R.id.editCostText);
        isPaid = findViewById(R.id.switchPaid);

        total = findViewById(R.id.totalEdit);
        total.setText("0");

        drawerlayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerlayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerlayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        int id = item.getItemId();
                        if (id == R.id.nav_add_item){
                            addItemToInvoice();
                            drawerlayout.closeDrawer(GravityCompat.START);
                            return true;
                        } else if (id == R.id.nav_clear_fields){
                            clearFields();
                            drawerlayout.closeDrawer(GravityCompat.START);
                            return true;
                        }
                        else if (id == R.id.nav_exit){
                            System.exit(0);
                        }
                        else if (id == R.id.nav_add_invoice){
                            drawerlayout.closeDrawer(GravityCompat.START);
                            addNewInvoice();
                        }
                        else if (id == R.id.nav_list_all_invoices){
                            Intent i = new Intent(MainActivity.this, AllInvoices.class);
                            startActivity(i);
                        }
                        else if (id == R.id.nav_google_maps){
                            Intent i = new Intent(MainActivity.this, GoogleMapsActivity.class);
                            startActivity(i);
                        }
                        return false;
                    }
                }
        );

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewInvoice();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.option_clear_all_fields) {
            clearFields();
        } else if (id == R.id.option_add_item) {
            addItemToInvoice();
        }
        // tell the OS
        return true;
    }
    private void clearFields(){
        issuerName.setText("");
        buyerName.setText("");
        buyerAddress.setText("");
        itemName.setText("");
        itemCost.setText("");
        itemQuantity.setText("");
        isPaid.setChecked(false);

    }
    private void addItemToInvoice(){
        if (itemName.getText().toString().equals("")
        || itemQuantity.getText().toString().equals("") || itemCost.getText().toString().equals("")){
            Toast.makeText(this, "NOT ALL ITEM FIELDS ARE FILLED", Toast.LENGTH_LONG).show();
        }
        else{
            String iName = itemName.getText().toString();
            int iQuantity = Integer.parseInt(itemQuantity.getText().toString());
            int iCost = Integer.parseInt(itemCost.getText().toString());
            if (iName.length() < 2){
                Toast.makeText(this, "THE ITEM NAME SHOULD BE AT LEAST 2 CHARACTERS LONG", Toast.LENGTH_LONG).show();
            }
            else{
                //ID
                String letters = String.valueOf(iName.charAt(0) + iName.charAt(1));
                String rand1 = String.valueOf((int) (Math.random() * 4));
                String rand2 = String.valueOf((int) (Math.random() * 4));
                String rand3 = String.valueOf((int) (Math.random() * 4));
                String rand4 = String.valueOf((int) (Math.random() * 4));

                String ID = "T" + letters + "-" + rand1 + rand2 + rand3 + rand4;
                Item i = new Item(ID, iName, iCost, iQuantity);
                itemsOfInvoice.add(i);
                itemAdapter.notifyDataSetChanged();
                int totalPrice = Integer.parseInt(total.getText().toString());
                totalPrice += iQuantity * iCost;
                total.setText(String.valueOf(totalPrice));
            }
        }
    }

    private void addNewInvoice(){
        if (issuerName.getText().toString().equals("") || buyerName.getText().toString().equals("")
                || buyerAddress.getText().toString().equals("")){
            Toast.makeText(this, "NOT ALL FIELDS HAVE BEEN ADDED", Toast.LENGTH_LONG).show();
        }
        else{
            if (itemsOfInvoice.isEmpty()) {
                addItemToInvoice();
                if (!itemsOfInvoice.isEmpty()) {
                    save();
                }
            }
            else {
                save();
            }
        }
    }

    private void save(){
        Invoice i = new Invoice();

        //RANDOM ID
        Random random = new Random();
        char rand1 = (char) ('a' + random.nextInt(26));
        char rand2 = (char) ('a' + random.nextInt(26));
        String randInt1 = String.valueOf((int) (Math.random() * 4));
        String randInt2= String.valueOf((int) (Math.random() * 4));
        String randInt3 = String.valueOf((int) (Math.random() * 4));
        String randInt4 = String.valueOf((int) (Math.random() * 4));


        String idInvoice = "I" + rand1 + rand2 + "-" + randInt1 + randInt2 + randInt3 + randInt4;

        i.setId(idInvoice);
        i.setIssuer(issuerName.getText().toString());
        i.setBuyer(buyerName.getText().toString());
        i.setBuyerAddress(buyerAddress.getText().toString());
        i.setPaid(isPaid.isChecked());
        i.setItems(itemsOfInvoice);
        i.setTotalPrice(Integer.parseInt(total.getText().toString()));
        invoices.add(i);
        mInvoiceViewModel.insert(i);
        resetItemsListAndClearFields();
    }
    private void resetItemsListAndClearFields(){
        clearFields();
        total.setText("0");
        itemsOfInvoice.clear();
        itemAdapter.notifyDataSetChanged();
    }

    public void openWikiOnClick(View v){
        String searchQuery = itemName.getText().toString();
        Uri uri = Uri.parse("https://en.wikipedia.org/w/index.php?search=" + Uri.encode(searchQuery));
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int pointerOne = 0;
        int pointerTwo = 0;
        gestureDetector.onTouchEvent(event);
       // mScaleDetector.onTouchEvent(event);
        int number = event.getPointerCount();
        if (number == 1){
            pointerOne = event.getPointerId(0);

        } else if (number == 2){
            pointerOne = event.getPointerId(0);
            pointerTwo = event.getPointerId(1);
        }
        event.getX(event.findPointerIndex(pointerOne));
        event.getX(event.findPointerIndex(pointerTwo));
          return true;

    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) { return true; }

        // My laptop has a touchscreen and double tap worked on the touch screen.
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            addNewInvoice();
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) { return true; }

        @Override
        public boolean onDown(MotionEvent e) { return true; }

        @Override
        public boolean onSingleTapUp(MotionEvent e) { return true; }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            int q, c;
            try{
                q = Integer.parseInt(itemQuantity.getText().toString());
            }
            catch(Exception e){
                q = 0;
            }
            try{
                c = Integer.parseInt(itemCost.getText().toString());
            }
            catch(Exception e){
                c = 0;
            }
            if (distanceX > 0) {
                // User scrolled left

                c += (int) distanceX / 3;
                itemCost.setText(String.valueOf(c));

            } else if (distanceX < 0) {
                // User scrolled right
                if (c == 0 || (c - (int) distanceX) <=0 ){
                    c = 0;
                    itemCost.setText(String.valueOf(c));
                }
                else{
                    c += (int) distanceX / 3;
                    itemCost.setText(String.valueOf(c));
                }
            }
            if (distanceY > 0) {
                // User scrolled up
                q += (int) distanceY / 3;
                itemQuantity.setText(String.valueOf(q));
            } else if (distanceY < 0) {
                // User scrolled down
                if (q == 0 || (q - (int) distanceY) <=0 ){
                    q = 0;
                    itemQuantity.setText(String.valueOf(q));
                }
                else{
                    q += (int) distanceY / 3;
                    itemQuantity.setText(String.valueOf(q));
                }
            }

            return true;
        }

        @Override
       public void onLongPress(MotionEvent e) { addItemToInvoice(); }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) { return true; }

    }


    ////////////////////////////////////
    class MyBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent){
            String message = intent.getStringExtra(MyReceiver.SMS_MSG_KEY);


            StringTokenizer initial = new StringTokenizer(message, ":");
            String code = "";
            String data = "";
            try{
                code = initial.nextToken();
                data = initial.nextToken();
            }
            catch(Exception ex){
                Toast.makeText(MainActivity.this, "UNREADABLE MESSAGE", Toast.LENGTH_LONG).show();
            }

            try{
                StringTokenizer tokenizerForData = new StringTokenizer(data, ";");
                if (code.equals("invoice")){
                    issuerName.setText(tokenizerForData.nextToken());
                    buyerName.setText(tokenizerForData.nextToken());
                    buyerAddress.setText(tokenizerForData.nextToken());
                    if (tokenizerForData.nextToken().equals("TRUE")){
                        isPaid.setChecked(true);
                    }
                    else{
                        isPaid.setChecked(false);
                    }
                }
                else if (code.equals("item")){
                    itemName.setText(tokenizerForData.nextToken());
                    int iq, ic;
                    try {
                        iq = Integer.parseInt(tokenizerForData.nextToken());
                        ic = Integer.parseInt(tokenizerForData.nextToken());
                        itemQuantity.setText(String.valueOf(iq));
                        itemCost.setText(String.valueOf(ic));
                    }
                    catch(Exception e){
                        Toast.makeText(MainActivity.this, "The format of the message is incorrect", Toast.LENGTH_LONG).show();
                    }
                }
                else if (code.equals("save")){
                    addNewInvoice();

                }
                else{
                    Toast.makeText(MainActivity.this, "The format of the message is incorrect", Toast.LENGTH_LONG).show();
                }
            }
            catch(Exception e){
                Toast.makeText(MainActivity.this, "The format of the message is incorrect", Toast.LENGTH_LONG).show();
            }
        }
    }

}