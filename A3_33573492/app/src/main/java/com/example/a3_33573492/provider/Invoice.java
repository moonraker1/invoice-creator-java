package com.example.a3_33573492.provider;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity(tableName = "invoices")
public class Invoice {

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name = "id")
    private String id;

    @NonNull
    @ColumnInfo(name = "issuer")
    private String issuer;

    @NonNull
    @ColumnInfo(name = "buyer")
    private String buyer;

    @NonNull
    @ColumnInfo(name="buyer_address")
    private String buyerAddress;

    @ColumnInfo(name="is_paid")
    private boolean isPaid;

    @ColumnInfo(name="items")
    private ArrayList<Item> items;

    @ColumnInfo(name="total_price")
    private int totalPrice;

    @NonNull
    public String getId() { return id; }

    public void setId(@NonNull String id) { this.id = id; }

    @NonNull
    public String getIssuer() { return issuer; }

    public void setIssuer(@NonNull String issuer) { this.issuer = issuer; }

    @NonNull
    public String getBuyer() { return buyer; }

    public void setBuyer(@NonNull String buyer) { this.buyer = buyer; }

    @NonNull
    public String getBuyerAddress() { return buyerAddress; }

    public void setBuyerAddress(@NonNull String buyerAddress) { this.buyerAddress = buyerAddress; }

    public boolean isPaid() { return isPaid; }

    public void setPaid(boolean paid) { isPaid = paid; }

    public ArrayList<Item> getItems() { return items; }

    public void setItems(ArrayList<Item> items) { this.items = items; }

    public int getTotalPrice() { return totalPrice; }

    public void setTotalPrice(int totalPrice) { this.totalPrice = totalPrice; }
}
