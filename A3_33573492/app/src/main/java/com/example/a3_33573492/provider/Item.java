package com.example.a3_33573492.provider;

public class Item {
    private String itemID;
    private String itemName;
    private int itemPrice;
    private int itemQuantity;

    public Item(String ID, String itemName, int itemPrice, int itemQuantity){
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemQuantity = itemQuantity;
        itemID = ID;
    }

    public String getItemID() { return itemID; }

    public void setItemID(String itemID) { this.itemID = itemID; }

    public String getItemName() { return itemName; }

    public void setItemName(String itemName) { this.itemName = itemName; }

    public int getItemPrice() { return itemPrice; }

    public void setItemPrice(int itemPrice) { this.itemPrice = itemPrice; }

    public int getItemQuantity() { return itemQuantity; }

    public void setItemQuantity(int itemQuantity) { this.itemQuantity = itemQuantity; }

}
