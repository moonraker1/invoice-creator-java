package com.example.a3_33573492.provider;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class InvoiceViewModel extends AndroidViewModel {

    private InvoiceRepository mRepo;
    private LiveData<List<Invoice>> mAllInvoices;

    public InvoiceViewModel(@NonNull Application application) {
        super(application);
        mRepo = new InvoiceRepository(application);
        mAllInvoices = mRepo.getAllInvoices();
    }

    public LiveData<List<Invoice>> getAllInvoices() {
        return mAllInvoices;
    }

    public void insert(Invoice i) {
        mRepo.insert(i);
    }

    public void delete(Invoice i) { mRepo.delete(i); }
}
