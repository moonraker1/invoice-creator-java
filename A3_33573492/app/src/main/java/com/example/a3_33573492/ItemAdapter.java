package com.example.a3_33573492;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a3_33573492.provider.Item;

import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {

    ArrayList<Item> items;

    public void setData(ArrayList<Item> data) {
        items = data;
    }


    @NonNull
    @Override
    public ItemAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false); //CardView inflated as RecyclerView list item
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemAdapter.ItemViewHolder holder, int position) {
        Item item = items.get(position);
        holder.iNameText.setText(item.getItemName());
        holder.iQuantityText.setText(String.valueOf(item.getItemQuantity()));
        holder.iCostText.setText(String.valueOf(item.getItemPrice()));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView iNameText, iQuantityText, iCostText;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            iNameText = itemView.findViewById(R.id.item_name_text);
            iQuantityText = itemView.findViewById(R.id.item_quantity_text);
            iCostText = itemView.findViewById(R.id.item_cost_text);
        }
    }
}
