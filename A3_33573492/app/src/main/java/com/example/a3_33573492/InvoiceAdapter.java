package com.example.a3_33573492;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a3_33573492.provider.Invoice;
import com.example.a3_33573492.provider.InvoiceViewModel;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.InvoiceViewHolder> {

    ArrayList<Invoice> invoices;
    InvoiceViewModel invoiceViewModel;

    public void setViewModel(InvoiceViewModel ivm) { invoiceViewModel = ivm; }
    public void setData(ArrayList<Invoice> data) {
        invoices = data;
    }

    @NonNull
    @Override
    public InvoiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_invoice, parent, false); //CardView inflated as RecyclerView list item
        return new InvoiceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull InvoiceViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.invoiceIDText.setText(invoices.get(position).getId());
        holder.issuerNameText.setText(invoices.get(position).getIssuer());
        holder.buyerNameText.setText(invoices.get(position).getBuyer());
        holder.invoiceTotalText.setText(String.valueOf(invoices.get(position).getTotalPrice()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar = Snackbar.make(v, "Delete the invoice?", Snackbar.LENGTH_LONG);
                snackbar.setAction("YES", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        invoiceViewModel.delete(invoices.get(position));
                    }
                });
                snackbar.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return invoices.size();
    }

    public class InvoiceViewHolder extends RecyclerView.ViewHolder {
        TextView invoiceIDText;
        TextView issuerNameText;
        TextView buyerNameText;
        TextView invoiceTotalText;
        public InvoiceViewHolder(@NonNull View itemView) {
            super(itemView);
            invoiceIDText=itemView.findViewById(R.id.invoice_id_text);
            issuerNameText=itemView.findViewById(R.id.issuer_name_text);
            buyerNameText=itemView.findViewById(R.id.buyer_name_text);
            invoiceTotalText=itemView.findViewById(R.id.invoice_total_text);
        }
    }
}
