package com.example.a3_33573492;
import com.example.a3_33573492.provider.Invoice;
import com.example.a3_33573492.provider.InvoiceViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

public class AllInvoices extends AppCompatActivity {

    RecyclerView recyclerViewInvoices;
    LinearLayoutManager layoutManager;
    InvoiceAdapter invoiceAdapter;

    ArrayList<Invoice> invoices = new ArrayList<>();
    InvoiceViewModel mInvoiceViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_invoices);

        layoutManager = new LinearLayoutManager(this);
        recyclerViewInvoices = findViewById(R.id.recyclerView);
        recyclerViewInvoices.setLayoutManager(layoutManager);

        mInvoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        invoiceAdapter = new InvoiceAdapter();
        invoiceAdapter.setViewModel(mInvoiceViewModel);
        invoiceAdapter.setData(invoices);
        recyclerViewInvoices.setAdapter(invoiceAdapter);


        mInvoiceViewModel.getAllInvoices().observe(this, newData ->{
            invoiceAdapter.setData((ArrayList<Invoice>) newData);
            invoiceAdapter.notifyDataSetChanged();
        });

    }
    public void backOnClick(View v){
        finish();
    }


}